//
//  UserProfile.h
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfile : NSObject

@property NSInteger userId;
@property NSString  *username;
@property NSDictionary *profile;
@property NSString  *deviceToken;
@property UIImage *profileImage;

+ (UserProfile *)currentUser;

@end
