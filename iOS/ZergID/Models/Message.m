//
//  Message.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "Message.h"
#import "UserProfile.h"

@implementation Message

+ (Message *)messageWithDict:(NSDictionary *)data
{
    Message *newMessage = [Message new];
    [newMessage initWithDict:data];
    return newMessage;
}

- (void)initWithDict:(NSDictionary *)data
{
    _messageId = [[data objectForKey:@"message_id"] integerValue];
    _senderId = [[data objectForKey:@"user_id"] integerValue];
    _senderName = [self firstToUpper:[data objectForKey:@"username"]];
    
    NSString *ristr = [data objectForKey:@"receiver_id"];
    if( [ristr respondsToSelector:@selector(integerValue)])
        _receiverId = [ristr integerValue];
    else
        _receiverId = -1;
   
    NSString *pmstr = [data objectForKey:@"parent_message_id"];
    if ([pmstr respondsToSelector:@selector(integerValue)])
        _parentMessageId = [pmstr integerValue];
    else
        _parentMessageId = -1;
    
    
    _createdDate = [data objectForKey:@"created_date"];
    NSString *cn = [data objectForKey:@"character_name"];
    NSString *gn = [data objectForKey:@"game_name"];
    if ([cn class] != [NSNull class] && [gn class] != [NSNull class])
        _characterName = [NSString stringWithFormat:@"%@ - %@", cn, gn];
    else
        _characterName = @"";
    _subject = [data objectForKey:@"subject"];
    NSString *evid = [data objectForKey:@"event_id"];
    if ([evid class] != [NSNull class])
        _eventId = [evid integerValue];
    else
        _eventId = -1;
    _message = [data objectForKey:@"message"];
    _isViewed = [[data objectForKey:@"is_message_viewed"] boolValue];
    
    if (_parentMessageId != -1)
        _isReply = YES;
    else
        _isReply = NO;
    
    if ([[data objectForKey:@"is_message_parent"] isEqualToString:@"1"])
        _isParent = YES;
    else
        _isParent = NO;
    
    /*
    NSString *userID = [NSString stringWithFormat:@"%d", (int)[UserProfile currentUser].userId];
    NSString *garbage = [data objectForKey:@"is_garbage_message" ];
    
    if( [garbage class] == [NSNull class] )
        _isDeleted = NO;
    else if( [garbage rangeOfString:userID].location != NSNotFound )
        _isDeleted = YES;*/
    
}

- (NSString *)firstToUpper:(NSString *)str
{
    if (str.length == 0)
        return str;
    else
        return [str stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[str substringToIndex:1] uppercaseString]];
}

@end
