//
//  Message.h
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property NSInteger messageId;
@property NSInteger senderId;
@property NSString  *senderName;
@property NSInteger receiverId;
@property NSInteger parentMessageId;
@property NSString  *createdDate;
@property NSString  *characterName;
@property NSString  *message;
@property NSString  *subject;
@property NSInteger eventId;
@property BOOL      isViewed;
@property BOOL      isReply;
@property BOOL      isParent;
@property BOOL      isDeleted;

+ (Message *)messageWithDict:(NSDictionary *)data;

- (void)initWithDict:(NSDictionary *)data;

@end
