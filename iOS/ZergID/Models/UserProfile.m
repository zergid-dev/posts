//
//  UserProfile.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "UserProfile.h"

static UserProfile *currentUser = nil;

@implementation UserProfile

+ (UserProfile *)currentUser
{
    if (currentUser == nil)
        currentUser = [UserProfile new];
    return currentUser;
}

@end
