//
//  TPKeyboardAvoidingScrollView.h
//  Estify
//
//  Created by Niklas Olsson on 26/08/14.
//  Copyright (c) 2014 niklas. All rights reserved.//

#import <UIKit/UIKit.h>
#import "UIScrollView+TPKeyboardAvoidingAdditions.h"

@interface TPKeyboardAvoidingScrollView : UIScrollView <UITextFieldDelegate, UITextViewDelegate>
- (void)contentSizeToFit;
- (BOOL)focusNextTextField;
- (void)scrollToActiveTextField;
@end
