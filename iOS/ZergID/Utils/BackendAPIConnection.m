//
//  BackendAPIConnection.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "BackendAPIConnection.h"

static BackendAPIConnection *sharedConnection;

@implementation BackendAPIConnection

+ (BackendAPIConnection *)sharedConnection
{
    if (sharedConnection == nil)
    {
        sharedConnection = [BackendAPIConnection new];
    }
    return sharedConnection;
}

- (id)init
{
    self = [super init];
    if (self)
    {
    }
    return self;
}

- (void)accessLoginAPI:(NSString *)username
              Password:(NSString *)password
     CompletionHandler:(void (^)(NSDictionary *result))handler
{
    NSString *apiPath = @"http://api.zergid.com/RestAPI/login_api";
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:username forKey:@"Username"];
    [params setObject:password forKey:@"Password"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Login");
        handler(result);
    }];
}

- (void)accessGetMessagesAPI:(NSInteger)userId
                        Page:(NSInteger)page
             MessagesPerPage:(NSInteger)limit
           CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = @"http://profileapi1.zergid.com:3000/v1/user/messages/%lu/%lu/%lu";
    apiPath = [NSString stringWithFormat:apiPath, (long)userId, (long)page, (long)limit];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Get %lu messages in page %lu", limit, page);
        handler(result, data, error);
    }];
}

- (void)accessGetThreadMessagesAPI:(NSInteger)parentMessageId
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = @"http://profileapi1.zergid.com:3000/v1/threads/message/%lu";
    apiPath = [NSString stringWithFormat:apiPath, (long)parentMessageId];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Get all thread messages in parent %lu", (long)parentMessageId);
        handler(result, data, error);
    }];
}

- (void)accessReadMessageMarkAPI:(NSInteger)messageId
               CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = @"http://profileapi1.zergid.com:3000/v1/message/%lu";
    apiPath = [NSString stringWithFormat:apiPath, (long)messageId];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Read message of id %lu", (long)messageId);
        handler(result, data, error);
    }];
}

/*
- (void)accessLoadMoreMessagesAPI:(NSInteger)userId
                    LastMessageId:(NSInteger)lastMsgId
                CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = @"http://api.zergid.com/RestAPI/load_more_messages";
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"UserId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)lastMsgId] forKey:@"LastMsgId"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Load more messages");
        handler(result, data, error);
    }];
}
*/
- (void)accessRegisterDeviceAPI:(NSInteger)userId
                    DeviceToken:(NSString *)deviceToken
              CompletionHandler:(void (^)(NSData *data, NSError *error))handler
{
    NSString *apiPath = @"http://profileapi1.zergid.com/v1/device/register";
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"user_id"];
    [params setObject:deviceToken forKey:@"device_id"];
    [params setObject:@"iOS" forKey:@"platform"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Device registration for push notification");
        handler(data, error);
    }];
}







@end
