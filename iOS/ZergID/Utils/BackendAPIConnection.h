//
//  BackendAPIConnection.h
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackendAPIConnection : NSObject 

+ (BackendAPIConnection *)sharedConnection;

- (id)init;

- (void)accessLoginAPI:(NSString *)username
              Password:(NSString *)password
     CompletionHandler:(void (^)(NSDictionary *result))handler;
- (void)accessGetMessagesAPI:(NSInteger)userId
                        Page:(NSInteger)page
             MessagesPerPage:(NSInteger)limit
           CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;
- (void)accessGetThreadMessagesAPI:(NSInteger)parentMessageId
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;
- (void)accessReadMessageMarkAPI:(NSInteger)messageId
               CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;
- (void)accessRegisterDeviceAPI:(NSInteger)userId
                    DeviceToken:(NSString *)deviceToken
              CompletionHandler:(void (^)(NSData *data, NSError *error))handler;


@end
