//
//  BackendBase.m
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "BackendBase.h"

@implementation BackendBase

- (void) accessAPI:(NSString *)apiPath
 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:apiPath]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults objectForKey:@"zergSession"] ){
        [request setValue:[defaults objectForKey:@"zergSession"] forHTTPHeaderField:@"Cookie"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (data != nil)
                               {
                                   if( [apiPath isEqualToString:[NSString stringWithFormat:@"%@%@", baseURLPHP, @"login_api"]] ){
                                       NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
                                       NSDictionary *fields = [HTTPResponse allHeaderFields];
                                       NSString *cookie = [fields valueForKey:@"Set-Cookie"];
                                       [defaults setObject:cookie forKey:@"zergSession"];
                                       [defaults synchronize];
                                   }

                                   
                                   NSError *myError = nil;
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:NSJSONReadingMutableLeaves error:&myError];
                                   handler(result, data, connectionError);
                               }
                               else
                               {
                                   handler(nil, nil, nil);
                               }
                           }];
}

- (void) accessAPI:(NSString *)apiPath
        Parameters:(NSDictionary *)params
 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *requestURL = [NSString stringWithFormat:@"%@?api", apiPath];
    for (NSString *key in params) {
        requestURL = [NSString stringWithFormat:@"%@&%@=%@", requestURL, key, [params objectForKey:key]];
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:requestURL]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults objectForKey:@"zergSession"] ){
        [request setValue:[defaults objectForKey:@"zergSession"] forHTTPHeaderField:@"Cookie"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (data != nil)
                               {
                                   if( [apiPath isEqualToString:[NSString stringWithFormat:@"%@%@", baseURLPHP, @"login_api"]] ){
                                       NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
                                       NSDictionary *fields = [HTTPResponse allHeaderFields];
                                       NSString *cookie = [fields valueForKey:@"Set-Cookie"];
                                       [defaults setObject:cookie forKey:@"zergSession"];
                                       [defaults synchronize];
                                   }

                                   
                                   NSError *myError = nil;
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:NSJSONReadingMutableLeaves error:&myError];
                                   handler(result, data, connectionError);
                               }
                               else
                               {
                                   handler(nil, nil, nil);
                               }
                           }];
}

- (void)accessAPIbyPOST:(NSString *)apiPath
             Parameters:(NSDictionary *)params
      CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *requestURL = [NSString stringWithFormat:@"%@", apiPath];
    NSString *postString = @"api";
    for (NSString *key in params) {
        postString = [NSString stringWithFormat:@"%@&%@=%@", postString, key, [params objectForKey:key]];
    }
    
    NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:requestURL]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    
    NSUserDefaults *defaults;
  
    defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults objectForKey:@"zergSession"] ){
        [request setValue:[defaults objectForKey:@"zergSession"] forHTTPHeaderField:@"Cookie"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (data != nil)
                               {
                                   if( [apiPath isEqualToString:[NSString stringWithFormat:@"%@%@", baseURLPHP, @"login_api"]] ){
                                       NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
                                       NSDictionary *fields = [HTTPResponse allHeaderFields];
                                       NSString *cookie = [fields valueForKey:@"Set-Cookie"];
                                       [defaults setObject:cookie forKey:@"zergSession"];
                                       [defaults synchronize];
                                   }

                                   
                                   NSError *myError = nil;
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:NSJSONReadingMutableLeaves error:&myError];
                                   handler(result, data, connectionError);
                               }
                               else
                               {
                                   handler(nil, nil, nil);
                               }
                           }];
}

- (void)uploadImagebyPost:(NSString *)apiPath
               Parameters:(NSDictionary *)params
                    Image:(UIImage*) image
                 filename:(NSString*)filename
        CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    
    NSString *requestURL = [NSString stringWithFormat:@"%@", apiPath];
    
    NSString *boundary = @"0xKhTmLbOuNdArY";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    
    //NSString *postString = @"api";
    //for (NSString *key in params) {
    //    postString = [NSString stringWithFormat:@"%@&%@=%@", postString, key, [params objectForKey:key]];
    //}
    
    //NSData *postData = [postString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"img.jpg\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];    
    [body appendData:[NSData dataWithData: UIImageJPEGRepresentation(image, 0.6f) ]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"api\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"" dataUsingEncoding:NSUTF8StringEncoding]];

    
    for (NSString *key in params) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[params objectForKey:key] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setURL:[NSURL URLWithString:requestURL]];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:body];
    
    NSUserDefaults *defaults;
    defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults objectForKey:@"zergSession"] ){
        [request setValue:[defaults objectForKey:@"zergSession"] forHTTPHeaderField:@"Cookie"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (data != nil)
                               {
                                   if( [apiPath isEqualToString:[NSString stringWithFormat:@"%@%@", baseURLPHP, @"login_api"]] ){
                                       NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
                                       NSDictionary *fields = [HTTPResponse allHeaderFields];
                                       NSString *cookie = [fields valueForKey:@"Set-Cookie"];
                                       [defaults setObject:cookie forKey:@"zergSession"];
                                       [defaults synchronize];
                                   }
                                   
                                   
                                   NSError *myError = nil;
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:NSJSONReadingMutableLeaves error:&myError];
                                   handler(result, data, connectionError);
                               }
                               else
                               {
                                   handler(nil, nil, nil);
                               }
                           }];
    
    
    

}


@end
