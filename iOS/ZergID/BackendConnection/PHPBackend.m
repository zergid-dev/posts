//
//  PHPBackend.m
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "PHPBackend.h"
#import "Message.h"
#import "UserProfile.h"

static PHPBackend   *sharedConnection;

@implementation PHPBackend
{
    NSInteger   lastMessageId;
}

+ (BackendBase *)sharedConnection
{
    if (sharedConnection == nil)
        sharedConnection = [PHPBackend new];
    return sharedConnection;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        lastMessageId = 0;
    }
    return self;
}

- (void)loginWithUsername:(NSString *)username
                 Password:(NSString *)password
        CompletionHandler:(void (^)(NSDictionary *result))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"login_api"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:username forKey:@"Username"];
    [params setObject:password forKey:@"Password"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Login");
        handler(result);
    }];
}

- (void)extendGetProfile:(NSString *)userid
       CompletionHandler:(void (^)(NSDictionary *result))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", extendURLPHP, @"profile/get"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:userid forKey:@"id"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Get Profile");
        handler(result);
    }];
}

- (void)extendPostFeed: (NSString *)feedtext
                 Image: (UIImage*) image
            youtubeURL: (NSString*) youtube_url
            reZergText: (NSString*)feed_rezergtext
              parentID: (NSString*) parent_id
                guidID: (NSString*)parent_guild_id
               shareAS: (NSString*)shareas
     CompletionHandler: (void (^)(NSDictionary *result))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", extendURLPHP, @"post/write"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    [params setObject:feedtext forKey:@"feedtext"];
    [params setObject:youtube_url forKey:@"youtube_url"];
    [params setObject:feed_rezergtext forKey:@"feed_rezergtext"];
    [params setObject:parent_id forKey:@"parent_id"];
    [params setObject:parent_guild_id forKey:@"parent_guild_id"];
    [params setObject:shareas forKey:@"shareas"];
    
    if( image != nil ){
        [self uploadImagebyPost:apiPath Parameters:params Image:image filename:@"media_icon" CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
            NSLog(@"PHP: Post Feed");
            handler(result);
        }];
    } else {
        [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
            NSLog(@"PHP: Post Feed");
            handler(result);
        }];
    }
}

- (void)getMessagesWithUserId:(NSInteger)userId
            CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"get_all_messages"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"UserId"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Get messages");
        if (result != nil)
        {
            NSArray *messages = [result objectForKey:@"messages"];
            if (messages != nil)
                if (messages.count > 0)
                {
                    Message *msg = [Message messageWithDict:[messages objectAtIndex:messages.count-1]];
                    lastMessageId = msg.messageId;
                }
        }
        handler(result, data, error);
    }];
}

- (void)loadMoreMessagesWithUserId:(NSInteger)userId
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"load_more_messages"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"UserId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)lastMessageId] forKey:@"LastMsgId"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Load more messages with last msg id: %lu", (long)lastMessageId);
        if (result != nil)
        {
            NSArray *messages = [result objectForKey:@"messages"];
            if (messages != nil)
                if (messages.count > 0)
                {
                    Message *msg = [Message messageWithDict:[messages objectAtIndex:messages.count-1]];
                    lastMessageId = msg.messageId;
                }
        }
        handler(result, data, error);
    }];
}

- (void)loadMoreMessagesWithUserId:(NSInteger)userId LastMessageID: (NSInteger)lastID
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"load_more_messages"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"UserId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)lastID] forKey:@"LastMsgId"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Load more messages with last msg id: %lu", (long)lastID);
        handler(result, data, error);
    }];
}


- (void)getThreadMessagesWithParentMsgId:(NSInteger)parentMessageId
                       CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"get_thread_messages"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)parentMessageId] forKey:@"parentmessageid"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Get all thread messages in parent %lu", (long)parentMessageId);
        handler(result, data, error);
    }];
}

- (void)readMessage:(NSInteger)messageId
  CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"readmessage_api"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)[UserProfile currentUser].userId] forKey:@"UserId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)messageId] forKey:@"MessageId"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Read message of id %lu", (long)messageId);
        handler(result, data, error);
    }];
}

- (void)registerDevice:(NSInteger)userId
           DeviceToken:(NSString *)deviceToken
     CompletionHandler:(void (^)(NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"registration_device"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"userid"];
    [params setObject:deviceToken forKey:@"deviceID"];
    [params setObject:@"iOS" forKey:@"platform"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Device registration for push notification");
        handler(data, error);
    }];
}

- (void)unRegisterDevice:(NSInteger)userId
           DeviceToken:(NSString *)deviceToken
     CompletionHandler:(void (^)(NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"unregistration_device"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    //[params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"userid"];
    [params setObject:deviceToken forKey:@"deviceID"];
    //[params setObject:@"iOS" forKey:@"platform"];
    [self accessAPI:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Device registration for push notification");
        handler(data, error);
    }];
}

- (void)replyMessage:(NSInteger)topMessageId
     ParentMessageId:(NSInteger)parentMsgId
          ReceiverId:(NSInteger)receiverId
             Message:(NSString *)message
   CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLPHP, @"replymessage_api"];
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)topMessageId] forKey:@"MessageId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)parentMsgId] forKey:@"ParentMessageId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)receiverId] forKey:@"ReceiverId"];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)[UserProfile currentUser].userId] forKey:@"SenderId"];
    [params setObject:message forKey:@"Message"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"PHP: Reply Message");
        handler(result, data, error);
    }];
}

@end
