//
//  BackendBase.h
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackendBase : NSObject

+ (BackendBase *)sharedConnection;

- (id)init;

- (void)loginWithUsername:(NSString *)username
                 Password:(NSString *)password
        CompletionHandler:(void (^)(NSDictionary *result))handler;

- (void)extendGetProfile:(NSString *)userid
        CompletionHandler:(void (^)(NSDictionary *result))handler;

- (void)extendPostFeed:(NSString *)feedtext
                 Image:(UIImage*) image
            youtubeURL: (NSString*) youtube_url
            reZergText: (NSString*)feed_rezergtext
              parentID: (NSString*) parent_id
                guidID:(NSString*)parent_guild_id
               shareAS:(NSString*)shareas
       CompletionHandler:(void (^)(NSDictionary *result))handler;

- (void)getMessagesWithUserId:(NSInteger)userId
            CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)loadMoreMessagesWithUserId:(NSInteger)userId
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)loadMoreMessagesWithUserId:(NSInteger)userId LastMessageID: (NSInteger)lastID
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)getThreadMessagesWithParentMsgId:(NSInteger)parentMessageId
                       CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)readMessage:(NSInteger)messageId
  CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)replyMessage:(NSInteger)topMessageId
     ParentMessageId:(NSInteger)parentMsgId
          ReceiverId:(NSInteger)receiverId
             Message:(NSString *)message
   CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)registerDevice:(NSInteger)userId
           DeviceToken:(NSString *)deviceToken
     CompletionHandler:(void (^)(NSData *data, NSError *error))handler;

- (void)unRegisterDevice:(NSInteger)userId
           DeviceToken:(NSString *)deviceToken
     CompletionHandler:(void (^)(NSData *data, NSError *error))handler;

/* Common functions for all backends */

- (void) accessAPI:(NSString *)apiPath
 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void) accessAPI:(NSString *)apiPath
        Parameters:(NSDictionary *)params
 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)accessAPIbyPOST:(NSString *)apiPath
             Parameters:(NSDictionary *)params
      CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;

- (void)uploadImagebyPost:(NSString *)apiPath
               Parameters:(NSDictionary *)params
                    Image:(UIImage*) image
                 filename:(NSString*)filename
      CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler;


@end
