//
//  NodeJSBackend.m
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "NodeJSBackend.h"

#define PAGE_MESSAGES_COUNT     25

static NodeJSBackend    *sharedConnection = nil;

@implementation NodeJSBackend
{
    NSInteger   currentPageCount;
}

+ (BackendBase *)sharedConnection
{
    if (sharedConnection == nil)
        sharedConnection = [NodeJSBackend new];
    return sharedConnection;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        currentPageCount = 0;
    }
    return self;
}

- (void)loginWithUsername:(NSString *)username
                 Password:(NSString *)password
        CompletionHandler:(void (^)(NSDictionary *result))handler
{
    NSString *apiPath = [NSString stringWithFormat: @"%@%@", baseURLPHP, @"login_api"];
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:username forKey:@"Username"];
    [params setObject:password forKey:@"Password"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Login");
        handler(result);
    }];
}

- (void)getMessagesWithUserId:(NSInteger)userId
            CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    currentPageCount = 1;
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"user/messages/%lu/%lu/%lu"];
    apiPath = [NSString stringWithFormat:apiPath, (long)userId, 1, PAGE_MESSAGES_COUNT];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Get messages");
        handler(result, data, error);
    }];
}

- (void)loadMoreMessagesWithUserId:(NSInteger)userId
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    currentPageCount++;
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"user/messages/%lu/%lu/%lu"];
    apiPath = [NSString stringWithFormat:apiPath, (long)userId, currentPageCount, PAGE_MESSAGES_COUNT];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Get messages");
        handler(result, data, error);
    }];
}

- (void)loadMoreMessagesWithUserId:(NSInteger)userId LastMessageID: (NSInteger)lastID
                 CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"user/messages/%lu/%lu/%lu"];
    apiPath = [NSString stringWithFormat:apiPath, (long)userId, currentPageCount, PAGE_MESSAGES_COUNT];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Get messages");
        handler(result, data, error);
    }];
}

- (void)getThreadMessagesWithParentMsgId:(NSInteger)parentMessageId
                       CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"threads/message/%lu"];
    apiPath = [NSString stringWithFormat:apiPath, (long)parentMessageId];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Get all thread messages in parent %lu", (long)parentMessageId);
        handler(result, data, error);
    }];
}

- (void)readMessage:(NSInteger)messageId
  CompletionHandler:(void (^)(NSDictionary *result, NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"message/%lu"];
    apiPath = [NSString stringWithFormat:apiPath, (long)messageId];
    [self accessAPI:apiPath CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Read message of id %lu", (long)messageId);
        handler(result, data, error);
    }];
}

- (void)registerDevice:(NSInteger)userId
           DeviceToken:(NSString *)deviceToken
     CompletionHandler:(void (^)(NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"device/register"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"user_id"];
    [params setObject:deviceToken forKey:@"device_id"];
    [params setObject:@"iOS" forKey:@"platform"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Device registration for push notification");
        handler(data, error);
    }];
}


- (void)unRegisterDevice:(NSInteger)userId
           DeviceToken:(NSString *)deviceToken
     CompletionHandler:(void (^)(NSData *data, NSError *error))handler
{
    NSString *apiPath = [NSString stringWithFormat:@"%@%@", baseURLRestAPI, @"device/unregister"];
    NSMutableDictionary *params = [NSMutableDictionary new];
    //[params setObject:[NSString stringWithFormat:@"%lu", (long)userId] forKey:@"user_id"];
    [params setObject:deviceToken forKey:@"device_id"];
    //[params setObject:@"iOS" forKey:@"platform"];
    [self accessAPIbyPOST:apiPath Parameters:params CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Node.js: Device registration for push notification");
        handler(data, error);
    }];
}


@end
