//
//  AppDelegate.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "AppDelegate.h"
#import "UserProfile.h"

#import "LoginViewController.h"
#import "InboxViewController.h"
#import "MessagesViewController.h"
#import "ReplyViewController.h"

#import "MessageBox.h"

@interface AppDelegate (){
    NotificationView *notificationView;
}

@end

@implementation AppDelegate
{

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [NRLogger setLogLevels:NRLogLevelInfo];
    [NewRelicAgent startWithApplicationToken:@"AAf8dade90339e6ba5baa58e05350692a0182151bd"];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    
    NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if( [defaults objectForKey:@"zergUserName"] )
        [UserProfile currentUser].username = [defaults objectForKey:@"zergUserName"];
    if( [defaults objectForKey:@"zergUserId"] ){
        [UserProfile currentUser].userId = [[defaults objectForKey:@"zergUserId"] integerValue];
        
        if([defaults objectForKey:@"zergProfile"] ){
            [UserProfile currentUser].profile = [defaults objectForKey:@"zergProfile"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString *profileImgUrl = [NSString stringWithFormat:@"%@%@", profileImageURL, [[UserProfile currentUser].profile objectForKey:@"profile_image1"]];
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:profileImgUrl]];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [UserProfile currentUser].profileImage = [UIImage imageWithData:imageData];
                });
            });
        
        
            if( remoteNotification){
                [self openMessageFromNotification:[remoteNotification objectForKey:@"aps_info"] isLaunched: YES];
            }
            else
                [self moveToMainNavigationController];
                
        }
    }

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    if([self.window.rootViewController isKindOfClass:[UINavigationController class]]){
        
        UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
        if( [[navController visibleViewController] isKindOfClass:[InboxViewController class]] ){
            InboxViewController *inboxVC = (InboxViewController*)[navController visibleViewController];
            [inboxVC reloadInbox:YES];
        }

    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
/*
   [self application:[UIApplication sharedApplication] didReceiveRemoteNotification:
     @{ @"aps": @{ @"alert": @"serendo: test from device", @"badge": @"+1", @"sound":@"default"}, @"aps_info" : @{@"parent_msg_id":@"19795", @"receiver_id": @"216", @"sender_id":@"238"}}];
 */
     
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)moveToLoginViewController
{
    UINavigationController *mainController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    self.window.rootViewController = mainController;
}

- (void)moveToMainNavigationController
{
    UINavigationController *mainController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    self.window.rootViewController = mainController;
    [mainController popToRootViewControllerAnimated:YES];
}

-(void)moveToRootViewController
{
    UINavigationController *rootController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
    self.window.rootViewController = rootController;
    [rootController popToRootViewControllerAnimated:YES];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)_deviceToken
{
    static NSString *deviceToken;
    deviceToken = [[NSString alloc] init];
    
    deviceToken = [[[[_deviceToken description]
                     stringByReplacingOccurrencesOfString: @"<" withString: @""]
                    stringByReplacingOccurrencesOfString: @">" withString: @""]
                   stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"My token is: %@", deviceToken);
    [UserProfile currentUser].deviceToken = [NSString stringWithFormat:@"%@", deviceToken];
    
    if( [UserProfile currentUser].userId ){
        BackendBase           *backend;
        backend = [PHPBackend sharedConnection];
        UserProfile *currentUser = [UserProfile currentUser];
        [backend registerDevice:currentUser.userId
                         DeviceToken:currentUser.deviceToken
                   CompletionHandler:^(NSData *data, NSError *error) {
                   }];
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //After analyzing userInfo, specified view should be shown
    NSLog(@"Received notification: %@", userInfo);
    
    if( notificationView == nil){
        notificationView = [[NotificationView alloc] init];
        notificationView.delegate = self;
    }
    
    
    if([self.window.rootViewController isKindOfClass:[UINavigationController class]]){
        
        UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
        
        //IF : app is running in background and Inbox view controller is showing
        if( application.applicationState == UIApplicationStateActive ){

            if( [[[navController viewControllers] objectAtIndex:0] isKindOfClass:[InboxViewController class]] ){
                InboxViewController *inboxVC = (InboxViewController*)[[navController viewControllers] objectAtIndex:0];
                [inboxVC reloadInbox : YES];
                
            }
            
            if( [[navController visibleViewController] isKindOfClass:[MessagesViewController class]] ){
                //If user is looking in sender's messages, new message should be appear in message box
                
                MessagesViewController *messageVC = (MessagesViewController*)[navController visibleViewController];
                NSDictionary *info = [userInfo objectForKey:@"aps_info"];
               
                if(![[info objectForKey:@"sender_id"] isKindOfClass:[NSNull class]] && messageVC.receiverId == [[info objectForKey:@"sender_id"] integerValue]){
                    [messageVC loadMessages];
                }
            }/* else if( [[navController visibleViewController] isKindOfClass:[ReplyViewController class]]){
                ReplyViewController *replyVC = (ReplyViewController*)[navController visibleViewController];
                NSDictionary *info = [userInfo objectForKey:@"aps_info"];
                
                if(![[info objectForKey:@"sender_id"] isKindOfClass:[NSNull class]] && replyVC.threadViewController.receiverId == [[info objectForKey:@"sender_id"] integerValue]){
                    [replyVC.threadViewController loadMessages];
                }
            }*/
            
            [self.window.rootViewController.view addSubview:notificationView];
            [notificationView hideNotification];
            notificationView.messageInfo = [userInfo objectForKey:@"aps_info"];
            [notificationView showMessage:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]];
            
            
        } else {
            [self openMessageFromNotification: [userInfo objectForKey:@"aps_info"] isLaunched: NO];
        }
    }
    
}

-(void) openMessage:(NSDictionary *)messageInfo
{
    [self openMessageFromNotification:messageInfo  isLaunched: NO];
}

-(void) openMessageFromNotification: (NSDictionary*) info isLaunched: (BOOL) isLaunched {
        
    NSString *idstr = [info objectForKey:@"receiver_id"];
   
    if( [UserProfile currentUser].userId != [idstr integerValue] ) return;
    
    UINavigationController *mainController = [self.window.rootViewController.storyboard
                                                          instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    
    [mainController popToRootViewControllerAnimated:NO];
    
    InboxViewController *inboxVC = (InboxViewController*)[[mainController viewControllers] objectAtIndex:0];
    if( inboxVC != nil && [inboxVC respondsToSelector:@selector(reloadInbox)])
    {
        [inboxVC reloadInbox:YES];
    }

    
    NSString *parent_message_id = [info objectForKey:@"parent_msg_id"];
    if( ![parent_message_id isEqualToString:@""] ){
        
        
        NSArray *params = [parent_message_id componentsSeparatedByString:@"/"];
        NSInteger parent_msg_id;
        BOOL hasError = YES;
        
        if( [params count] == 1 ){
            parent_msg_id = [params[0] integerValue];
            if ( [params[0] isEqualToString:[NSString stringWithFormat:@"%lu", (long)parent_msg_id]] ) hasError = NO;
        }
        else if( [params count] == 4 ){
            parent_msg_id = [params[3] integerValue];
            if( [params[1] isEqualToString:@"message"] && [params[2] isEqualToString:@"index"] ) hasError = NO;
        }
        
        if( hasError == NO && ![[info objectForKey:@"sender_id"] isKindOfClass:[NSNull class]]){
            MessagesViewController *messageVC = [self.window.rootViewController.storyboard
                                                 instantiateViewControllerWithIdentifier:@"messageController"];

            messageVC.parentMessageId = parent_msg_id;
            messageVC.receiverId = [[info objectForKey:@"sender_id"] integerValue];
            
            [mainController pushViewController:messageVC animated:YES];
        }
        
    }
    
    self.window.rootViewController = mainController;
}

    
@end
