//
//  MessagesViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "MessagesViewController.h"
#import "ReplyViewController.h"
#import "InboxCell.h"
#import "MBProgressHUD.h"
#import "UserProfile.h"

@interface MessagesViewController () <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *tableViewMessages;
    __weak IBOutlet NSLayoutConstraint *backButtonBottomSpaceConstraint;
}
@end

@implementation MessagesViewController

- (void)viewDidLoad
{
    self.bottomSpaceConstraint = backButtonBottomSpaceConstraint;
    
    [super viewDidLoad];
    
    tableViewMessages.dataSource = self;
    tableViewMessages.delegate = self;
    
    
}

-(void) loadMessages{
    _messages = [NSMutableArray new];
    
    if(_selectedMessage)
        _selectedMessage.isViewed = true;
    
    NSInteger parentMsgId = _parentMessageId;
    if (parentMsgId == -1)
        parentMsgId = _topMessageId;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.backend getThreadMessagesWithParentMsgId:parentMsgId CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        
        if (result == nil)
            return;
        NSArray *newMessages = [result objectForKey:@"messages"];
        if (newMessages != nil)
        {
            for (int i=0; i<newMessages.count; i++)
            {
                Message *message = [Message messageWithDict:[newMessages objectAtIndex:i]];
                if( !message.isDeleted ) [_messages addObject:message];
            }
            [tableViewMessages reloadData];
        }
        [self.backend readMessage:parentMsgId CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
            /// No need to do something here
        }];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadMessages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([tableViewMessages respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableViewMessages setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableViewMessages respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableViewMessages setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (IBAction)onTappedBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat) tableView: (UITableView*) tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InboxCell *cell = [tableViewMessages dequeueReusableCellWithIdentifier:@"InboxCellId"];
    Message *message = [_messages objectAtIndex:indexPath.row];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width - 72, 30)];
    label.numberOfLines = 0;
    label.font = cell.labelMessage.font;
    label.text = message.message;
    [label sizeToFit];
    NSLog(@"%f, %f", label.frame.size.height, label.frame.size.width);
    return label.frame.size.height + 82;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _messages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InboxCell *cell = [tableViewMessages dequeueReusableCellWithIdentifier:@"InboxCellId"];
    Message *message = [_messages objectAtIndex:indexPath.row];
    cell.labelUsername.text = message.senderName;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [format dateFromString:message.createdDate];
    NSTimeInterval gmtTimeZoneSecnods = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSTimeInterval estTimeZoneSeconds = gmtTimeZoneSecnods + 5*60*60;
    date = [date dateByAddingTimeInterval:estTimeZoneSeconds];
    [format setDateFormat:@"yyyy-MM-dd hh:mm a"];
    
    cell.labelDate.text = [format stringFromDate:date];
    
    //cell.labelDate.text = message.createdDate;
    cell.labelCharacterName.text = message.characterName;
    cell.labelMessage.text = message.message;
    cell.isMessageViewed = true;
    cell.isReplied = message.receiverId != [UserProfile currentUser].userId;
    
    [cell initCell];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - URL Detection in MSG

-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSLog(@"%@", URL);
    return YES;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ReplyViewController *replyViewController = (ReplyViewController *)[segue destinationViewController];
    replyViewController.messageId = _topMessageId;
    NSInteger parentMsgId = _parentMessageId;
    if (parentMsgId == -1)
        parentMsgId = _topMessageId;
    replyViewController.parentMsgId = parentMsgId;
    replyViewController.receiverId = _receiverId;
    replyViewController.threadViewController = self;
}


@end
