//
//  MessagesViewController.h
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Message.h"

@interface MessagesViewController : BaseViewController <UITextViewDelegate>

@property NSInteger         parentMessageId;
@property NSInteger         topMessageId;
@property NSInteger         receiverId;
@property Message           *selectedMessage;
@property NSMutableArray    *messages;

-(void) loadMessages;
@end
