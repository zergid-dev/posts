//
//  ReplyViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "ReplyViewController.h"
#import "SVProgressHUD.h"
#import "MessageBox.h"

@interface ReplyViewController ()
{
    __weak IBOutlet UITextView *textViewReply;
    __weak IBOutlet NSLayoutConstraint *textViewReplyBottomSpace;
    
    __weak IBOutlet UIToolbar *keyboardToolBar;    
    __weak IBOutlet NSLayoutConstraint *keyboardToolBarConstraint;
}

@end

@implementation ReplyViewController

- (void)viewDidLoad {
    self.bottomSpaceConstraint = textViewReplyBottomSpace;
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardOnScreen:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDismissed:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTappedCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onTappedReply:(id)sender {
    
    NSString *message = textViewReply.text;
    
    if( [message isEqualToString:@""] ) return;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    [self.backend replyMessage:_messageId
                                ParentMessageId:_parentMsgId
                                     ReceiverId:_receiverId
                                        Message:message
                              CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        NSLog(@"Reply message api response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

        [SVProgressHUD dismiss];
        [self.navigationController popViewControllerAnimated:YES];
        
    }];
}

- (void)keyboardOnScreen:(NSNotification *)notification
{
    NSDictionary *info  = notification.userInfo;
    NSValue      *value = info[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    textViewReplyBottomSpace.constant = keyboardFrame.size.height + keyboardToolBar.bounds.size.height;
    
    [self.view bringSubviewToFront:keyboardToolBar];
    
    NSLog(@"%f, %f", keyboardFrame.size.height, keyboardToolBar.bounds.size.height);
    
    keyboardToolBar.hidden = NO;
    [UIView animateWithDuration:0.25f animations:^{
        keyboardToolBarConstraint.constant = - keyboardFrame.size.height - keyboardToolBar.bounds.size.height;
    }];
}

- (void)keyboardDismissed:(NSNotification *)notification
{
    textViewReplyBottomSpace.constant = self.adBannerView.frame.size.height;
    keyboardToolBar.hidden = YES;
}

- (IBAction)onDone:(id)sender {
    [self.view endEditing:YES];
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
