//
//  PostViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 16/12/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "PostViewController.h"
#import "ReviewPostViewController.h"
#import "AppDelegate.h"
#import "UserProfile.h"

#import "UIImage+fixOrientation.h"
#import "FHSTwitterEngine.h"

@interface PostViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, FHSTwitterEngineAccessTokenDelegate, UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, ADBannerViewDelegate>{
    
    
    NSString *youtubeURL;
    UIImage *image;
    NSString *shareAS;
    NSInteger attachmentType;
    
    NSString *characterString;
    CGFloat defaultFeedContainerHeight;
    
    __weak IBOutlet UIButton *btnTwitter;
    __weak IBOutlet UITextView *txtFeed;
    
    __weak IBOutlet UIToolbar *keyboardToolBar;
    __weak IBOutlet NSLayoutConstraint *feedContainerHeightConstraint;
    
    __weak IBOutlet UIScrollView *contentScrollView;
    
    __weak IBOutlet UIButton *btnShareAs;
    __weak IBOutlet UIView *shareContainerView;
    __weak IBOutlet UIPickerView *sharePickerView;
    __weak IBOutlet NSLayoutConstraint *shareContainerViewBottomSpaceConstraint;
}

@end

@implementation PostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    characterString = @"";
    shareAS = @"";
    
    self.bottomSpaceConstraint = shareContainerViewBottomSpaceConstraint;
    
    shareContainerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    defaultFeedContainerHeight = txtFeed.bounds.size.height + 10;
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:kTwitterConsumerKey andSecret:kTwitterSecretKey];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
    [[FHSTwitterEngine sharedEngine]loadAccessToken];
    
    txtFeed.text = @"Write something for your Zerg feed!";
    txtFeed.textColor = [UIColor lightGrayColor];
    txtFeed.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initialize
{
    characterString = @"";
    shareAS = @"";
    
    txtFeed.text = @"Write something for your Zerg feed!";
    txtFeed.textColor = [UIColor lightGrayColor];
    attachmentType = 0;
    
    [btnShareAs setTitle:@"Share As" forState:UIControlStateNormal];
    [btnTwitter setSelected:NO];
}

#pragma mark - TextView Delegate

-(BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if([txtFeed.textColor isEqual:[UIColor lightGrayColor]]) txtFeed.text = @"";
    txtFeed.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    if(txtFeed.text.length == 0){
        txtFeed.textColor = [UIColor lightGrayColor];
        txtFeed.text = @"Write something for your Zerg feed!";
        [txtFeed resignFirstResponder];
    }
}

-(void) textViewDidEndEditing:(UITextView *)textView
{
    
    if(txtFeed.text.length == 0){
        txtFeed.textColor = [UIColor lightGrayColor];
        txtFeed.text = @"Write something for your Zerg feed!";
        [txtFeed resignFirstResponder];
    }
}



#pragma mark - Attachment

- (IBAction)onAttachMedia:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Attach Image or Youtube Video URL" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take a Picture", @"Choose from Gallery", @"Attach Youtube Video", nil];
    
    [actionSheet showInView:self.view];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Take a Picture"])
    {
        if ([UIImagePickerController isSourceTypeAvailable:  UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = [NSArray arrayWithObjects:  (NSString *) kUTTypeImage,  nil];
            imagePicker.allowsEditing = NO;
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self presentViewController:imagePicker animated:YES completion:nil];
            }];
            
        }
        
        
    } else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Choose from Gallery"]) {
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString*)kUTTypeImage, nil];
        imagePicker.allowsEditing = NO;
        imagePicker.delegate = self;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self presentViewController:imagePicker animated:YES completion:nil];
        }];
        
    } else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Attach Youtube Video"]) {
        
        UIAlertView *inputAlertView = [[UIAlertView alloc] initWithTitle:@"Input Youtube URL" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        
        inputAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        inputAlertView.tag = 1000;
        [inputAlertView show];
    }
    
}

#pragma mark - Input Youtube URL
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if( alertView.tag == 1000 ){ //Attachment
        
        if( [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"OK"] ){
            attachmentType = kVideoAttachment;
            youtubeURL = [alertView textFieldAtIndex:0].text;
        }
        
    } else { //Logout
        
        if( [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Logout"] ){
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if( [defaults objectForKey:@"zergUserId"] )
                [defaults removeObjectForKey:@"zergUserId"];
            if( [defaults objectForKey:@"zergUserName"] )
                [defaults removeObjectForKey:@"zergUserName"];
            [defaults synchronize];
            
            [[FHSTwitterEngine sharedEngine] clearAccessToken];
            
            UserProfile *currentUser = [UserProfile currentUser];
            if (currentUser.deviceToken != nil)
            {
                [self.backend unRegisterDevice:currentUser.userId
                                   DeviceToken:currentUser.deviceToken
                             CompletionHandler:^(NSData *data, NSError *error) {
                                 
                                 AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                                 [appDelegate moveToLoginViewController];
                                 
                             }];
            } else{
                AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appDelegate moveToLoginViewController];
            }
            
        }         
    }
}


#pragma mark - Twitter

- (IBAction)onCheckTwitter:(id)sender {
    
    if( btnTwitter.selected ){
        btnTwitter.selected = NO;
        return;
    }
    
    if( [[FHSTwitterEngine sharedEngine]isAuthorized] ){
        btnTwitter.selected = YES;
        return;
    }
    
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
        [btnTwitter setSelected:success];
    }];
    [self presentViewController:loginController animated:YES completion:nil];
    
}

- (void)storeAccessToken:(NSString *)accessToken {
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:@"SavedAccessHTTPBody"];
}

- (NSString *)loadAccessToken {
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"SavedAccessHTTPBody"];
}


#pragma mark - Attach Image

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
        image = [image fixOrientation];
        attachmentType = kImageAttachment;
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)onPreview:(id)sender {

    
    if( attachmentType != kImageAttachment && attachmentType != kVideoAttachment && ([txtFeed.textColor isEqual:[UIColor lightGrayColor]] ||
                                                                                     [txtFeed.text isEqualToString:@""])){
        
        UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"Please input feed or add attachment" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [msgView show];
        
        return;
    }
    
    [self performSegueWithIdentifier:@"segueReviewPost" sender:self];
    
}

#pragma mark - Keyboard

- (IBAction)onDone:(id)sender {
    [self.view endEditing:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    
    feedContainerHeightConstraint.constant = defaultFeedContainerHeight;
    keyboardToolBar.hidden = YES;
    
}

- (void)keyboardDidShow:(NSNotification *)notification{
    
    CGSize size = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    float height = MIN(size.height, size.width);
    
    float topConstantHeight = 66.0f;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) topConstantHeight = 86.0f;
    
    if( self.navigationController.view.bounds.size.height - height - keyboardToolBar.bounds.size.height - txtFeed.bounds.size.height - topConstantHeight < 0){
        feedContainerHeightConstraint.constant = MAX(self.navigationController.view.bounds.size.height - height - keyboardToolBar.bounds.size.height -
                                                     topConstantHeight, 30);
        
        [contentScrollView setContentOffset:CGPointMake(0, -39) animated:YES];
        
    }else
        feedContainerHeightConstraint.constant = defaultFeedContainerHeight;
    
    
    keyboardToolBar.hidden = NO;
    [UIView animateWithDuration:0.25f animations:^{
        keyboardToolBar.center = CGPointMake(keyboardToolBar.center.x, self.navigationController.view.bounds.size.height  - height - keyboardToolBar.frame.size.height/2);
    }];
    
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.view endEditing:YES];
}

#pragma mark - Share AS

- (IBAction)onOpenShareAs:(id)sender {
    
    if( shareContainerView.hidden == NO ) return;
    
    shareContainerView.alpha = 0;
    shareContainerView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        shareContainerView.alpha = 1.0f;
    }];
    
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSDictionary *profile = [UserProfile currentUser].profile;
    
    return [profile[@"chars"] count] + 1;
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary *profile = [UserProfile currentUser].profile;
    
    if( row == 0 ) return [UserProfile currentUser].username;
    
    NSDictionary *character = [profile[@"chars"] objectAtIndex:row - 1];
    return [NSString stringWithFormat:@"%@ - %@", character[@"character_name"], character[@"game_name"]];
}

- (IBAction)onChooseShareAs:(id)sender {
    
    NSInteger selectedRow = [sharePickerView selectedRowInComponent:0];
    
    if( selectedRow == 0 ){
        shareAS = @"";
        characterString = [UserProfile currentUser].username;
        [btnShareAs setTitle:[NSString stringWithFormat:@"Share As: %@", characterString] forState:UIControlStateNormal];
    } else {
        NSDictionary *character = [[[UserProfile currentUser].profile objectForKey:@"chars"] objectAtIndex:selectedRow-1];
        shareAS = [character objectForKey:@"character_detail_id"];
        characterString = [NSString stringWithFormat:@"%@ - %@", character[@"character_name"], character[@"game_name"]];
        [btnShareAs setTitle:[NSString stringWithFormat:@"Share As: %@", characterString] forState:UIControlStateNormal];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        shareContainerView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        shareContainerView.hidden = YES;
    }];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if( [segue.identifier isEqualToString:@"segueReviewPost"] ){
        
        ReviewPostViewController* reviewVC = (ReviewPostViewController*)segue.destinationViewController;
        
        if( [txtFeed.textColor isEqual:[UIColor lightGrayColor]] )
            reviewVC.comment = @"";
        else
            reviewVC.comment = txtFeed.text;
        
        reviewVC.attachmentType = attachmentType;
        reviewVC.shareWithTwitter = btnTwitter.selected;
        
        if( attachmentType == kImageAttachment )
            reviewVC.attachment = image;
        else if(attachmentType == kVideoAttachment)
            reviewVC.attachment = youtubeURL;
        
        reviewVC.shareAs = shareAS;
        reviewVC.characterDetail = characterString;
        reviewVC.postVC = self;
    }
}

- (IBAction)moveToInbox:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate moveToMainNavigationController];
}

- (IBAction)onLogout:(id)sender {
    
    UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"" message:@"Do you really want to logout?" delegate:self cancelButtonTitle:@"Logout" otherButtonTitles:@"Cancel", nil];
    
    [msgView show];
}


@end
