//
//  BaseViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController () <ADBannerViewDelegate>
{
    CGFloat bannerHeight;
    BOOL constraintImpemented;
    NSLayoutConstraint *bannerHeightConstraint;
}

@end

@implementation BaseViewController

@synthesize backend;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //backend = [NodeJSBackend sharedConnection];
    backend = [PHPBackend sharedConnection];

    CGRect viewFrame = self.view.bounds;
    CGRect bannerFrame;
    bannerHeight = 50;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) bannerHeight = 66;
    
    bannerFrame.size = CGSizeMake(viewFrame.size.width, bannerHeight);
    bannerFrame.origin.x = 0;
    bannerFrame.origin.y = viewFrame.size.height - bannerHeight;
    _adBannerView = [[ADBannerView alloc] initWithFrame:bannerFrame];
    
    constraintImpemented = NO;
    
    _adBannerView.delegate = self;
    _adBannerView.hidden = YES;
    [self.view addSubview:_adBannerView];
    
    [self.view setNeedsUpdateConstraints];
    
}

-(void) updateViewConstraints
{
    
    [self setupConstraints];
    [super updateViewConstraints];
    
}

-(void) setupConstraints
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if( UIInterfaceOrientationIsPortrait(self.interfaceOrientation) )
        {
            bannerHeight = 50;
        }else
            bannerHeight = 32;
    }
    
    if( constraintImpemented == NO ){
    
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem: _adBannerView
                                                              attribute:NSLayoutAttributeLeft
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeLeft
                                                             multiplier:1.0
                                                               constant:0]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem: _adBannerView
                                                              attribute:NSLayoutAttributeRight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeRight
                                                             multiplier:1.0
                                                               constant:0]];
        
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem: _adBannerView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1.0
                                                               constant:0]];
        bannerHeightConstraint = [NSLayoutConstraint constraintWithItem: _adBannerView
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.0
                                                               constant:bannerHeight];
        
        [self.view addConstraint:bannerHeightConstraint];
        constraintImpemented = YES;
    } else {
    
        [self.view removeConstraint:bannerHeightConstraint];
        
        bannerHeightConstraint = [NSLayoutConstraint constraintWithItem: _adBannerView
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1.0
                                                               constant:bannerHeight];
        [self.view addConstraint:bannerHeightConstraint];
    }
    
    self.adBannerView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if( self.adBannerView.hidden == NO ) _bottomSpaceConstraint.constant = bannerHeight;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"Ad loaded");
    _adBannerView.hidden = NO;
    if (_bottomSpaceConstraint != nil)
        _bottomSpaceConstraint.constant = bannerHeight;
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Failed to retrieve ad");
    _adBannerView.hidden = YES;
    if (_bottomSpaceConstraint != nil)
        _bottomSpaceConstraint.constant = 0;
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.view setNeedsUpdateConstraints];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
