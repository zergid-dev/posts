//
//  ReplyViewController.h
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "MessagesViewController.h"

@interface ReplyViewController : BaseViewController

@property NSInteger     receiverId;
@property NSInteger     messageId;
@property NSInteger     parentMsgId;
@property MessagesViewController    *threadViewController;

@end
