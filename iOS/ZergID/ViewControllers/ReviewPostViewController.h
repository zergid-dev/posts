//
//  ReviewPostViewController.h
//  ZergID
//
//  Created by Oleg Koshkin on 16/12/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "BaseViewController.h"
#import "PostViewController.h"

@interface ReviewPostViewController : BaseViewController

@property NSInteger attachmentType;
@property BOOL shareWithTwitter;
@property (nonatomic, strong) NSString* comment;
@property NSObject *attachment;
@property NSString *shareAs;
@property NSString *characterDetail;
@property (nonatomic, retain) PostViewController *postVC;

@end
