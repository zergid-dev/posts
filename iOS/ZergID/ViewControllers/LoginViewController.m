//
//  LoginViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "LoginViewController.h"
#import "UserProfile.h"
#import "SVProgressHUD.h"
#import "MessageBox.h"
#import "AppDelegate.h"

@interface LoginViewController ()
{
    __weak IBOutlet UITextField *txtUsername;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet NSLayoutConstraint *backImageBottomSpaceConstraint;
    __weak IBOutlet UIButton *btnRememberMe;    
    __weak IBOutlet UIImageView *imgBackgroundView;
    BOOL rememberMe;
}

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    self.bottomSpaceConstraint = nil;
    rememberMe = YES;
    
    if( [[UIScreen mainScreen] bounds].size.width <=480 && [[UIScreen mainScreen] bounds].size.height <= 480 ) //iPhone4 size
        imgBackgroundView.image = [UIImage imageNamed:@"bg-4.png"];
        
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickRememberMe:(id)sender {
    rememberMe = !rememberMe;
    
    if( rememberMe )
       [btnRememberMe setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    else
        [btnRememberMe setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    
}

- (void)loginSuccess:(NSDictionary *)result
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if( rememberMe ){
        [defaults setObject:[result objectForKey:@"id"] forKey:@"zergUserId"];
        [defaults setObject:txtUsername.text forKey:@"zergUserName"];
        
    } else {
        if( [defaults objectForKey:@"zergUserId"] )
            [defaults removeObjectForKey:@"zergUserId"];
        if( [defaults objectForKey:@"zergUserName"] )
            [defaults removeObjectForKey:@"zergUserName"];
    }
    [defaults synchronize];
    
    NSString *idstr = [result objectForKey:@"id"];
    
    [self.backend extendGetProfile:idstr CompletionHandler:^(NSDictionary *profile) {
        
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate moveToRootViewController];
        
        if( profile == nil ) return;
        if( [profile[@"data"] isKindOfClass:[NSNull class]] ) return;
        
        NSDictionary *profile_data = profile[@"data"];
        NSLog(@"**** Profile: %@", profile_data);
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if( rememberMe ){
            [defaults setObject:profile_data forKey:@"zergProfile"];
        } else if( [defaults objectForKey:@"zergProfile"])
            [defaults removeObjectForKey:@"zergProfile"];
        
        [defaults synchronize];
        UserProfile *currentUser = [UserProfile currentUser];
        currentUser.profile = profile_data;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSString *profileImgUrl = [NSString stringWithFormat:@"%@%@", profileImageURL, profile_data[@"profile_image1"]];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:profileImgUrl]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                currentUser.profileImage = [UIImage imageWithData:imageData];
            });
        });
    }];
    
    UserProfile *currentUser = [UserProfile currentUser];
    currentUser.userId = [idstr integerValue];
    currentUser.username = txtUsername.text;
    
    if (currentUser.deviceToken != nil){
        
        [self.backend registerDevice:currentUser.userId
                         DeviceToken:currentUser.deviceToken
                   CompletionHandler:^(NSData *data, NSError *error) {
        }];
    }
    
}

- (IBAction)onTappedLogin:(id)sender {
    if ([txtUsername.text isEqualToString:@""])
    {
        [MessageBox alert:@"Please enter username."];
        return;
    }
    if ([txtPassword.text isEqualToString:@""])
    {
        [MessageBox alert:@"Please enter password."];
        return;
    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];

    [self.backend loginWithUsername:txtUsername.text
                      Password:txtPassword.text
             CompletionHandler:^(NSDictionary *result) {
        [SVProgressHUD dismiss];
        if (result != nil)
            [self loginSuccess:result];
        else
          [MessageBox alert:@"Please enter the correct password"];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
