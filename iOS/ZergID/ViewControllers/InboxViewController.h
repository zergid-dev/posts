//
//  InboxViewController.h
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface InboxViewController : BaseViewController <UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray      *messages;

- (void)reloadInbox : (BOOL) forceRefresh;
- (void)refreshInbox;

@end
