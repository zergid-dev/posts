//
//  BaseViewController.h
//  ZergID
//
//  Created by Oleg Koshkin on 26/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import "NodeJSBackend.h"
#import "PHPBackend.h"

@interface BaseViewController : UIViewController

@property ADBannerView          *adBannerView;
@property NSLayoutConstraint    *bottomSpaceConstraint;
@property BackendBase           *backend;

@end
