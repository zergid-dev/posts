//
//  InboxViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "AppDelegate.h"
#import "InboxViewController.h"
#import "MessagesViewController.h"
#import "InboxCell.h"
#import "UserProfile.h"
#import "Message.h"
#import "SVPullToRefresh.h"
#import "MessageBox.h"
#import "MBProgressHUD.h"

#define PAGE_MESSAGE_COUNT   25

@interface InboxViewController () <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *tableViewInbox;
    
    UIRefreshControl    *refreshControl;
    __weak IBOutlet NSLayoutConstraint *tableViewBottomSpaceConstraint;
    
    __weak IBOutlet UILabel *labelNotification;
    
}
@end

@implementation InboxViewController

- (void)viewDidLoad
{
    self.bottomSpaceConstraint = tableViewBottomSpaceConstraint;
    
    [super viewDidLoad];
    
    tableViewInbox.delegate = self;
    tableViewInbox.dataSource = self;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [tableViewInbox addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(reloadInbox:) forControlEvents:UIControlEventValueChanged];
    
    [tableViewInbox addInfiniteScrollingWithActionHandler:^{
        [self loadMore];
    }];
    
    _messages = [NSMutableArray new];

}

-(void) viewDidAppear:(BOOL)animated
{
    if( [_messages count] == 0 ) [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [self reloadInbox : NO];
    
}


-(void)refreshInbox
{
    [tableViewInbox reloadData];
}

- (void)reloadInbox : (BOOL) forceRefresh
{
    NSIndexPath *path  = [NSIndexPath indexPathForRow:0 inSection:0];
    if( !forceRefresh && [[tableViewInbox indexPathsForVisibleRows] count] > 0 )
        path = (NSIndexPath*)[[tableViewInbox indexPathsForVisibleRows] objectAtIndex:0];
    
    if( path.row == 0 ){
        [self.backend getMessagesWithUserId:[UserProfile currentUser].userId
                     CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {

            [MBProgressHUD hideHUDForView:self.navigationController.view animated:NO];
            [refreshControl endRefreshing];
                      
            //Add New Messages
            if (result != nil){
                NSArray *allMessages = [result objectForKey:@"messages"];
                             
                if (allMessages)
                {
                    if( _messages.count == 0 ){
                        for (int i=0; i<allMessages.count; i++) {
                             NSDictionary *dicItem = [allMessages objectAtIndex:i];
                             Message *message = [Message messageWithDict:dicItem];
                             [_messages addObject:message];
                         }
                    }else {
                        int cnt;
                        for (cnt=0; cnt<allMessages.count; cnt++) {
                            NSDictionary *dicItem = [allMessages objectAtIndex:cnt];
                            Message *message = [Message messageWithDict:dicItem];
                            if( message.messageId == ((Message *)_messages[0]).messageId )
                                break;
                        }
                        
                        for(int i = cnt-1; i >=0; i--)
                        {
                            NSDictionary *dicItem = [allMessages objectAtIndex:i];
                            Message *message = [Message messageWithDict:dicItem];
                            [_messages insertObject:message atIndex:0];
                        }
                        
                        NSRange removeRange;
                        removeRange.location = cnt;
                        removeRange.length = [_messages count] > [allMessages count]? [allMessages count]-cnt : [_messages count] - cnt;
                        
                        [_messages removeObjectsInRange:removeRange];
                        
                        for (int i=cnt; i<allMessages.count; i++) {
                            NSDictionary *dicItem = [allMessages objectAtIndex:i];
                            Message *message = [Message messageWithDict:dicItem];
                            [_messages insertObject:message atIndex:i];
                        }

                    
                    }
                    
                    [tableViewInbox reloadData];
                }
            }
            
             if( _messages.count > 0 )
                 labelNotification.hidden = YES;
             else
                 labelNotification.hidden = NO;
        }];
    } else
        [self refreshVisibleMessages];

    
    
}

-(void) refreshVisibleMessages
{
    //Replace Visible Messages
        
    NSIndexPath *path = (NSIndexPath*)[[tableViewInbox indexPathsForVisibleRows] objectAtIndex:0];
    Message *msg = (Message*)[_messages objectAtIndex:path.row];
    
    [self.backend loadMoreMessagesWithUserId:[UserProfile currentUser].userId LastMessageID:msg.messageId+1
                           CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
                               
                               NSArray *allMessages = [result objectForKey:@"messages"];
                               if (allMessages)
                               {
                                   NSRange removeRange;
                                   removeRange.location = path.row;
                                   removeRange.length = [_messages count] - path.row > [allMessages count] ? [allMessages count] : [_messages count] - path.row;
                                   
                                   [_messages removeObjectsInRange:removeRange];
                                   
                                   for (int i=0; i<allMessages.count; i++) {
                                       NSDictionary *dicItem = [allMessages objectAtIndex:i];
                                       Message *message = [Message messageWithDict:dicItem];
                                       [_messages insertObject:message atIndex:path.row + i];
                                   }
                                   [tableViewInbox reloadData];
                               }
                               
                           }];
    
    
}

- (void)loadMore
{
    if( [_messages count] == 0 ) return;
    Message *msg = (Message*)[_messages lastObject];
    
    [self.backend loadMoreMessagesWithUserId:[UserProfile currentUser].userId LastMessageID:msg.messageId
                      CompletionHandler:^(NSDictionary *result, NSData *data, NSError *error) {
        
        [tableViewInbox.infiniteScrollingView stopAnimating];
                          
        
        NSArray *allMessages = [result objectForKey:@"messages"];
        if (allMessages)
        {
            for (int i=0; i<allMessages.count; i++) {
                NSDictionary *dicItem = [allMessages objectAtIndex:i];
                Message *message = [Message messageWithDict:dicItem];
                //if ([self isMessageFilteredForInbox:message])
                    [_messages addObject:message];
            }
            [tableViewInbox reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews
{
    if ([tableViewInbox respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableViewInbox setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableViewInbox respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableViewInbox setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (IBAction)onTappedLogout:(id)sender {
    
    UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"" message:@"Do you really want to logout?" delegate:self cancelButtonTitle:@"Logout" otherButtonTitles:@"Cancel", nil];
    
    [msgView show];
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( [[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Logout"] ){
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if( [defaults objectForKey:@"zergUserId"] )
            [defaults removeObjectForKey:@"zergUserId"];
        if( [defaults objectForKey:@"zergUserName"] )
            [defaults removeObjectForKey:@"zergUserName"];
        [defaults synchronize];
        
        UserProfile *currentUser = [UserProfile currentUser];
        if (currentUser.deviceToken != nil)
        {
            [self.backend unRegisterDevice:currentUser.userId
                             DeviceToken:currentUser.deviceToken
                       CompletionHandler:^(NSData *data, NSError *error) {
                           
                           AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                           [appDelegate moveToLoginViewController];
                           
            }];
        } else{
            AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appDelegate moveToLoginViewController];
        }
        
    }
}

#pragma mark - TableView DataSource and Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    InboxCell *cell = [tableViewInbox dequeueReusableCellWithIdentifier:@"InboxCellId"];
    Message *message = [_messages objectAtIndex:indexPath.row];
    cell.labelUsername.text = message.senderName;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [format dateFromString:message.createdDate];
    NSTimeInterval gmtTimeZoneSecnods = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSTimeInterval estTimeZoneSeconds = gmtTimeZoneSecnods + 5*60*60;
    date = [date dateByAddingTimeInterval:estTimeZoneSeconds];
    
    [format setDateFormat:@"yyyy-MM-dd hh:mm a"];
    
    cell.labelDate.text = [format stringFromDate:date];
    cell.labelCharacterName.text = message.characterName;
    cell.labelMessage.text = message.message;
    cell.isMessageViewed = message.isViewed;
    cell.isReplied = message.isReply;
    
    [cell initCell];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _messages.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"ViewMessageSegue" sender:self];
}


#pragma mark - Navigation

- (IBAction)moveToPostVC:(id)sender {
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate moveToRootViewController];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    Message *selectedMessage = (Message *)[_messages objectAtIndex:[tableViewInbox indexPathForSelectedRow].row];
    
    MessagesViewController *msgViewController = (MessagesViewController *)[segue destinationViewController];
    msgViewController.parentMessageId = selectedMessage.parentMessageId;
    msgViewController.topMessageId = selectedMessage.messageId;
    msgViewController.selectedMessage = selectedMessage;
    msgViewController.receiverId = selectedMessage.senderId;
/*
    NSMutableArray *threadMessages = [NSMutableArray new];
    if (selectedMessage.parentMessageId != -1)
    {
        for (int i=0; i<messages.count; i++)
            if ([[messages objectAtIndex:i] parentMessageId] == selectedMessage.parentMessageId)
                [threadMessages addObject:[messages objectAtIndex:i]];
    }
    else
        [threadMessages addObject:selectedMessage];
    msgViewController.messages = threadMessages;
*/
}


@end
