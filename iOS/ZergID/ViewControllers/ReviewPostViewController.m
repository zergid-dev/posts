//
//  ReviewPostViewController.m
//  ZergID
//
//  Created by Oleg Koshkin on 16/12/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "ReviewPostViewController.h"
#import "UserProfile.h"
#import "FHSTwitterEngine.h"
#import "HCYoutubeParser.h"
#import "MBProgressHUD.h"

@interface ReviewPostViewController () <UIAlertViewDelegate>{
    
    __weak IBOutlet UIImageView *profileImageView;
    __weak IBOutlet UILabel *lblUsername;
    __weak IBOutlet UILabel *lblDate;
    __weak IBOutlet UILabel *lblComment;
    
    __weak IBOutlet UIImageView *attachmentView;    
    __weak IBOutlet UIView *videoContainerView;
    
    UIImageView *imageView;
    MPMoviePlayerController *playerVC;
    
    __weak IBOutlet NSLayoutConstraint *commentHeight;
    __weak IBOutlet NSLayoutConstraint *contentViewHeight;
    __weak IBOutlet NSLayoutConstraint *submitBtnVSpaceConstraint;
    
    __weak IBOutlet NSLayoutConstraint *bottomConstraint;
    
    BOOL hasError;
}

@end

@implementation ReviewPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    hasError = NO;
    self.bottomSpaceConstraint = bottomConstraint;
    
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
    formatter.timeZone = destinationTimeZone;
    [formatter setDateStyle:NSDateFormatterLongStyle];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString* dateString = [formatter stringFromDate:date];
    
    UserProfile *currentUser = [UserProfile currentUser];
    profileImageView.image = currentUser.profileImage;
    
    lblUsername.text = [NSString stringWithFormat:@"%@\n%@", currentUser.username, self.characterDetail];
    lblDate.text     = dateString;
    lblComment.text  = _comment;
    
    
    if(_attachmentType == kImageAttachment ){
        attachmentView.image = (UIImage*)_attachment;
    } else if( _attachmentType == kVideoAttachment ){
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        NSURL *url = [NSURL URLWithString:(NSString*)_attachment];
        
        [HCYoutubeParser thumbnailForYoutubeURL:url thumbnailSize:YouTubeThumbnailDefaultHighQuality completeBlock:^(UIImage *image, NSError *error) {
            
            if (!error) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        attachmentView.image = image;
                    });
                });
                
                
                [HCYoutubeParser h264videosWithYoutubeURL:url completeBlock:^(NSDictionary *videoDictionary, NSError *error) {
                    
                   [MBProgressHUD hideHUDForView:self.view animated:NO];
                    NSDictionary *qualities = videoDictionary;
                    
                    NSString *URLString = nil;
                    if ([qualities objectForKey:@"small"] != nil) {
                        URLString = [qualities objectForKey:@"small"];
                    }
                    else if ([qualities objectForKey:@"live"] != nil) {
                        URLString = [qualities objectForKey:@"live"];
                    }
                    else {
                        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Couldn't find youtube video" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
                        return;
                    }
                    
                    playerVC = [[MPMoviePlayerController alloc] init];
                    playerVC.contentURL = [NSURL URLWithString:URLString];
                    playerVC.view.frame = attachmentView.bounds;
                    playerVC.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                    playerVC.scalingMode = MPMovieScalingModeAspectFit;
                    [videoContainerView addSubview:[playerVC view]];
                    playerVC.shouldAutoplay = NO;
                    playerVC.controlStyle = MPMovieControlStyleDefault;
                    [playerVC prepareToPlay];
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didVideoLoaded:) name:MPMovieMediaTypesAvailableNotification object:nil];
                    
                }];
            } else {
                hasError = YES;
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Incorrect Youtube URL" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [msgView show];
            }
        }];
    
    } else{
        attachmentView.hidden = YES;
    }
    
}

-(void) didVideoLoaded : (id) result{
    NSLog(@"Video Loaded: %@", result);
    videoContainerView.hidden = NO;
    attachmentView.hidden = YES;
}

-(void) viewWillAppear:(BOOL)animated
{
    [self viewWillLayoutSubviews];
}

- (IBAction)onBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) viewWillLayoutSubviews{
    [lblComment sizeToFit];
    commentHeight.constant = lblComment.bounds.size.height;
    
    if( _attachmentType == kImageAttachment || _attachmentType == kVideoAttachment )
        contentViewHeight.constant = 120 + lblComment.frame.origin.y + lblComment.bounds.size.height + attachmentView.bounds.size.height;
    else {
        contentViewHeight.constant = 90 + lblComment.frame.origin.y + profileImageView.bounds.size.height + lblComment.bounds.size.height;
        submitBtnVSpaceConstraint.constant = -attachmentView.bounds.size.height;
    }
}


- (IBAction)onSubmit:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.backend extendPostFeed:_comment
                           Image: (_attachmentType == kImageAttachment ? (UIImage*)_attachment : nil)
                      youtubeURL: (_attachmentType == kVideoAttachment ? (NSString*)_attachment : @"")
                      reZergText:@""
                        parentID:@""
                          guidID:@""
                         shareAS:_shareAs
               CompletionHandler:^(NSDictionary *result){
                   
                   [MBProgressHUD hideHUDForView:self.view animated:NO];
                   if( result != nil && [result[@"data"] isEqualToString:@"Success"] ){
                       UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"Post" message:@"Post a comment successfully done" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                       
                       [msgView show];
                       if( _shareWithTwitter ) [self postTwitter];
                   } else {
                       hasError = YES;
                       UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error occures when posting a feed." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                       
                       [msgView show];
                   }
               }];    
}

-(void) postTwitter{
    NSError *error;
    if( _attachmentType == kImageAttachment ){
        NSData *data = UIImageJPEGRepresentation((UIImage*)_attachment, 1.0f);
        error = [[FHSTwitterEngine sharedEngine] postTweet:_comment withImageData:data];
    }else if( _attachmentType == kVideoAttachment ){
        NSMutableString *commentWithVideo = [[NSMutableString alloc] init];
        
        [commentWithVideo appendString:_comment];
        [commentWithVideo appendString:@"\n"];
        [commentWithVideo appendString:(NSString*)_attachment];
        
        error = [[FHSTwitterEngine sharedEngine] postTweet:commentWithVideo];
        
    } else
        error = [[FHSTwitterEngine sharedEngine] postTweet:_comment];
    
    
    NSLog(@"%@", error);
    if( error == nil ){
        UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"Post" message:@"Post a comment successfully done" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [msgView show];
    } else {
        hasError = YES;
        UIAlertView *msgView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error occures when posting a feed on twitter." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [msgView show];
    }
}


-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( hasError == NO ) [_postVC initialize];
    [self.navigationController popViewControllerAnimated:YES];
}


@end
