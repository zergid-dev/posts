//
//  PostViewController.h
//  ZergID
//
//  Created by Oleg Koshkin on 16/12/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "BaseViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PostViewController : BaseViewController

-(void) initialize;

@end
