//
//  InboxCell.m
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import "InboxCell.h"

@implementation InboxCell
{
    __weak IBOutlet NSLayoutConstraint *replyMarkLeftPadding;    
    __weak IBOutlet NSLayoutConstraint *replyMarkWidthConstraint;
    __weak IBOutlet UIView *unreadMarkView;
    __weak IBOutlet UIView *replyMarkView;
}

- (void)awakeFromNib
{
    // Initialization code ..    
}

- (void)initCell
{

    if (_isMessageViewed)
    {
        unreadMarkView.hidden = YES;
        replyMarkLeftPadding.constant = 0;
    }
    else
    {
        unreadMarkView.hidden = NO;
        replyMarkLeftPadding.constant = unreadMarkView.frame.size.width;
    }
    
    replyMarkView.hidden = !_isReplied;
    
    if( _isReplied )
        replyMarkWidthConstraint.constant = 20;
    else
        replyMarkWidthConstraint.constant = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if (selected)
        self.contentView.backgroundColor = [UIColor colorWithRed:56.0/255.0 green:203.0/255.0 blue:240.0/255.0 alpha:1];
    else
        self.contentView.backgroundColor = [UIColor whiteColor];
}

@end
