//
//  NotificationView.h
//  ZergID
//
//  Created by Oleg Koshkin on 09/10/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NotificationView;

@protocol NotificationDelegate

-(void) openMessage : (NSDictionary*) messageInfo;

@end

@interface NotificationView : UIView

@property NSDictionary *messageInfo;
@property (nonatomic, assign) id delegate;

-(void) showMessage: (NSString*) msg;
-(void) hideNotification;

@end
