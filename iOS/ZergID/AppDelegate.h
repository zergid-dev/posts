//
//  AppDelegate.h
//  ZergID
//
//  Created by Oleg Koshkin on 25/09/14.
//  Copyright (c) 2014 ZergID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, NotificationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)moveToLoginViewController;
- (void)moveToMainNavigationController;
-(void)moveToRootViewController;

@end

