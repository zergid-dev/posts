<?php

class Pushnotification {

    const APNS_ERROR_RESPONSE_SIZE = 6;
    const APNS_RESPONSE_COMMAND = 8;
    const STATUS_CODE_INTERNAL_ERROR = 999;

    protected $_apnsErrorResponseMessages = array(
        0 => 'No errors encountered',
        1 => 'Processing error',
        2 => 'Missing device token',
        3 => 'Missing topic',
        4 => 'Missing payload',
        5 => 'Invalid token size',
        6 => 'Invalid topic size',
        7 => 'Invalid payload size',
        8 => 'Invalid token',
        self::STATUS_CODE_INTERNAL_ERROR => 'Internal error'
    ); /*     * < @type array Error-response messages. */

    private static function get_device_id($user_id, $platform) {
        return Model::factory("Device")->get_user_device_id($user_id, $platform);
    }

    private static function send_android($user_id, $subject, $message) {
        $apiKey = "AIzaSyBcv0aSSk5FROHnGFPaRbzaKAdZ_kLISSE";
// Replace with real client registration IDs 

        $registrationIDs = self::get_device_id($user_id, 'Android');

        if (!is_array($registrationIDs) || empty($registrationIDs)) {
            return false;
        }

// Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registrationIDs,
            //'data' => array("message" => $subject, "subject" => $message),
            'data' => array("message" => $message, "subject" => $subject),
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

// Open connection
        $ch = curl_init();

// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

// Execute post
        $result = curl_exec($ch);

        switch (curl_errno($ch)) {
            case null: Log::instance()->add(Log::INFO, 'GCM message send successful for user_id ' . $user_id . ' Response:' . $result);
                break; //Valid response
            case 28: Log::instance()->add(Log::ERROR, 'Timeout calling ' . $url);
                break;
            default: Log::instance()->add(Log::ERROR, 'info =>' . $ch);
                break;
        }

// Close connection
        curl_close($ch);
    }

    private static function send_ios($user_id, $message, $sender_id = '', $parent_message_id = '') {

        $push_method = 'live';
        $certPass = '';
        $badge = 1;
        $sound = 'default';
        $alert = $message;

        if ($push_method == 'develop') {
            $ssl_gateway_url = 'ssl://gateway.sandbox.push.apple.com: 2195';
            $certFile = APPPATH . '../apns_cert.pem';
        } else if ($push_method == 'live') {
            $ssl_gateway_url = 'ssl://gateway.push.apple.com: 2195';
            $certFile = APPPATH . '../apns_cert_pro.pem';
        }

        //$body[$custom_key] = $custom_value; 
        $aps_info = array();
        $aps_info['sender_id'] = $sender_id;
        $aps_info['receiver_id'] = $user_id;
        $aps_info['parent_msg_id'] = $parent_message_id;
        
        $body['aps_info'] = $aps_info;

        $registrationIDs = self::get_device_id($user_id, 'iOS');

        foreach ($registrationIDs as $deviceToken) {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $certFile);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $certPass);

            if (isset($certFile) && isset($ssl_gateway_url)) {
                $fp = stream_socket_client($ssl_gateway_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
                Log::instance()->add(Log::INFO, 'Connect success');
            }

            if (!$fp) {
                Log::instance()->add(Log::ERROR, 'Connect failed');
                return FALSE;
            }

            Log::instance()->add(Log::INFO, 'Device token: ' . $deviceToken);
            $deviceToken = str_replace(" ", "", $deviceToken);
            $deviceToken = pack('H*', $deviceToken);

            $tmp = array();
            if ($alert) {
                $tmp['alert'] = $alert;
            }
            if ($badge) {
                $tmp['badge'] = "+" . $badge;
            }
            if ($sound) {
                $tmp['sound'] = $sound;
            }
            $body['aps'] = $tmp;

            $payload = json_encode($body);
            $msg = chr(0) . chr(0) . chr(32) . $deviceToken . chr(0) . chr(strlen($payload)) . $payload;

            fwrite($fp, $msg);
            fclose($fp);
        }
    }

//    private static function send_ios($user_id, $message) {
//        $badge = 1;
//        $sound = 'default';
//        $development = false;
//
//        $payload = array();
//        $payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound);
//        $payload = json_encode($payload);
//
//        $apns_url = NULL;
//        $apns_cert = NULL;
//        $apns_port = 2195;
//        $apns_timeout = 60;
//        // Put your private key's passphrase here:
//        $passphrase = '';
//
//        $registrationIDs = self::get_device_id($user_id, 'iOS');
//
//        if ($development) {
//            $apns_url = 'gateway.sandbox.push.apple.com';
//            $apns_cert = APPPATH . '../apns_cert.pem';
//        } else {
//            $apns_url = 'gateway.push.apple.com';
//            $apns_cert = APPPATH . '../apns_cert_pro.pem';
//        }
//
//        Log::instance()->add(Log::INFO, 'apns_cert path: ' . $apns_cert);
//        $stream_context = stream_context_create();
//        stream_context_set_option($stream_context, 'ssl', 'local_cert', $apns_cert);
//        stream_context_set_option($stream_context, 'ssl', 'passphrase', $passphrase);
//        stream_context_set_option($stream_context, 'ssl', 'cafile', APPPATH . '../entrust_2048_ca.cer');
//
//        $apns = stream_socket_client('ssl://' . $apns_url . ':' . $apns_port, $error, $error_string, $apns_timeout, STREAM_CLIENT_CONNECT, $stream_context);
//        if (!$apns) {
//            Log::instance()->add(Log::ERROR, 'Socket creation to APNS failed: ' . $error_string . ' Error Num: ' . $error);
//        } else {
//            foreach ($registrationIDs as $device_token) {
//                $apns_message = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $device_token)) . chr(0) . chr(strlen($payload)) . $payload;
//                Log::instance()->add(Log::INFO, 'Sent push notification IOS Message ' . $apns_message);
//                fwrite($apns, $apns_message);
//                Log::instance()->add(Log::INFO, 'Sent push notification to device ' . $device_token);
//                Log::instance()->add(Log::INFO, 'Sent push notification to device of user ' . $user_id);
//                /* $sErrorResponse = @fread($apns, self::STATUS_CODE_INTERNAL_ERROR); //defaut error code not APPLE
//                  if ($sErrorResponse === false || strlen($sErrorResponse) != self::APNS_ERROR_RESPONSE_SIZE) {
//                  Log::instance()->add(Log::INFO, 'NO APNS Response');
//                  } else {
//                  Log::instance()->add(Log::INFO, 'APNS Response : ' . $sErrorResponse);
//                  } */
//            }
//        }
//
//        @socket_close($apns);
//        @fclose($apns);
//    }

    public static function send_feedback_request() {
        //connect to the APNS feedback servers
        //make sure you're using the right dev/production server & cert combo!
        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', APPPATH . '../apns_cert.pem');
        $apns = stream_socket_client('ssl://feedback.push.apple.com:2196', $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $stream_context);
        if (!$apns) {
            echo "ERROR $errcode: $errstr\n";
            return;
        }


        $feedback_tokens = array();
        //and read the data on the connection:
        while (!feof($apns)) {
            $data = fread($apns, 38);
            if (strlen($data)) {
                $feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
            }
        }
        fclose($apns);
        foreach ($feedback_tokens as $deviceid) {
            Model::factory('Device')->delete_device($deviceid);
        }
        return $feedback_tokens;
    }

    private static function send_noty($user_id, $sender_userid, $subject, $message, $callback_url) {
        $sendor_user = Model::factory('User')->get_user($sender_userid);

        $config_path = Kohana::$config->load('myconf');

        if (!$sendor_user->profile_image_active) {
            $src = '/' . $config_path->path['default_image_path'];
        } else {
            $profile_image = $config_path->profile_image[$sendor_user->profile_image_active];
            $profile_image = $sendor_user->$profile_image;
            $src = $config_path->path['cloud_path_image'] . $config_path->path['profile'] . $profile_image;
        }

        $url_send = Helper::get_noty_domain();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_send); // set url to post to 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable 
        curl_setopt($ch, CURLOPT_TIMEOUT, 3); // times out after 4s 
        curl_setopt($ch, CURLOPT_POST, 1); // set POST method 
        curl_setopt($ch, CURLOPT_POSTFIELDS, "subject=" . $subject . "&msg=" . $message . "&sendor_icon=" . $src . "&sendor_username=" . $sendor_user->username . "&userid=" . $user_id . "&callback_url=" . $callback_url); // add POST fields 
        $result = curl_exec($ch);
        curl_close($ch);  // Seems like good practice
    }

    private function readAPNSErrorMessage() {
        //TBD Detailed parsing of the APNS sErrorResponse using $_apnsErrorResponseMessages
    }

    public static function send($sender_userid, $user_id, $subject, $message, $callback_url = '') {
        $sendor_user = Model::factory('User')->get_user($sender_userid);

        $message = $sendor_user->username . ': ' . $message;
        Log::instance()->add(Log::INFO, 'Attempting Push to all mobile devices of user_id ' . $user_id);
        self::send_android($user_id, $subject, $message);
        //self::send_ios($user_id, $message);
        $parent_message_id = $callback_url;
        self::send_ios($user_id, $message, $sender_userid, $parent_message_id);
        self::send_noty($user_id, $sender_userid, $subject, $message, $callback_url);
//        echo $result;
    }

    public static function checkFeedbackServer($useDev = FALSE) {
        $apnsPort = 2196;

        if ($useDev) {
            echo 'FEEDBACK in DEVELOPER MODE <br/>';
            $apnsHost = 'feedback.sandbox.push.apple.com';
        } else {
            echo 'FEEDBACK in DISTRIBUTION MODE <br/>';
            $apnsHost = 'feedback.push.apple.com';
        }
        $finalPath = 'ssl://' . $apnsHost . ':' . $apnsPort;

        echo 'OPENING STREAM TO -> ' . $finalPath . '<br/>';


        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', APPPATH . '../apns_cert.pem');
        stream_context_set_option($stream_context, 'ssl', 'passphrase', 'qwert12345');
        stream_context_set_option($stream_context, 'ssl', 'cafile', APPPATH . '../entrust_2048_ca.cer');

        $apns = stream_socket_client($finalPath, $error, $errorString, 60, STREAM_CLIENT_CONNECT, $stream_context);

        if (!$apns) {
            echo "ERROR $error: $errorString\n";
            return;
        }
        else
            echo 'APNS FEEDBACK CONNECTION ESTABLISHED...<br/>';

        $feedback_tokens = array();
        $count = 0;

        echo 'error= ' . $error . '<br/>';
        echo 'errorString= ' . $errorString . '<br/>';

        if (!feof($apns))
            echo 'APNS NOT FINISHED <br/>';
        else
            echo 'APNS FINISHED? <br/>';

        $result = fread($apns, 38);
        echo 'result= ' . $result;
        fclose($apns);
    }

}

?>
