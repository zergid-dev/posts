<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_RestAPI extends Controller {

    public function action_index() {

        $feeds = ORM::factory('feeds')
                        ->select('zid_users.*')
                        // ->where('feeds.id','=','2') 
                        ->join('zid_users', 'INNER')
                        ->on('zid_users.id', '=', 'feeds.id')
                        ->order_by('feeds.feed_id', 'ASC')
                        ->limit(10)
                        ->find_all()->as_array();
        foreach ($feeds as $feed):
            $sa[] = $feed->as_array();
        endforeach;

        $this->response->headers('Content-Type', 'application/json');
        $this->response->body(json_encode($sa));
        $this->auto_render = FALSE;
    }

    public function action_login_api() {

//        if (HTTP_Request::POST == $this->request->method()) {
//        if (isset($_POST['Username']) && $_POST['Password']){
        $data = 0;
//            $username = "mind";            
//            $password = "mohan123";

        $username = $_POST['Username'];
        $password = $_POST['Password'];
        $user_isactive = ORM::factory('User')->where('username', '=', $username)->find();
        if ($user_isactive->is_active == NULL):
            $login = Auth::instance()->login($username, $password, $data);

            if ($login) {
                $user = Auth::instance()->get_user();
                $message_id = DB::select('message_id')->from(ORM::factory('Message')->table_name())->where('guild_id', '=', NULL)->and_where('event_id', '=', NULL)->where_open()->where('user_id', '=', $user->id)->or_where('receiver_id', '=', $user->id)->where_close()->and_where('parent_message_id', '=', NULL)->and_where('is_message_active', '=', 1)->limit(50)->order_by('message_id', 'DESC')->execute()->as_array();
                $data = count($message_id);
                if ($data > 0):
                    $messages = Model::factory('Message')->get_all_messages_api($user, $message_id)->as_array();
                    foreach ($messages as $message):
                        $message->message = preg_replace('!\(?[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]\)?!', '', $message->message);
                        $sa[] = $message->as_array();
                    endforeach;
                else:
                    $sa[] = "{}";
                endif;
                $this->response->headers('Content-Type', 'application/json');
                echo '{  "id":"' . $user->id . '","username":"' . $user->username . '","messages":' . json_encode($sa) . '}';
            }
            else {
                echo "Failed";
            }
        endif;
        $this->auto_render = FALSE;
    }

    public function action_get_all_messages() {
//            $user_id = "31";
        $user_id = $_GET['UserId'];
        $message_id = DB::select('message_id')->from(ORM::factory('Message')->table_name())->where('guild_id', '=', NULL)->and_where('event_id', '=', NULL)->where_open()->where('user_id', '=', $user_id)->or_where('receiver_id', '=', $user_id)->where_close()->and_where('parent_message_id', '=', NULL)->and_where('is_message_active', '=', 1)->limit(25)->order_by('message_id', 'DESC')->execute()->as_array();
        $data = count($message_id);
        if ($data > 0):
            $messages = Model::factory('Message')->get_all_messages_api($user_id, $message_id);
            $messages->as_array();
            foreach ($messages as $message):
                $message->message = preg_replace('!\(?[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]\)?!', '', $message->message);
                $sa[] = $message->as_array();
            endforeach;
        else:
            $sa[] = "{}";
        endif;
        $this->response->headers('Content-Type', 'application/json');
        echo '{"messages":' . json_encode($sa) . '}';
        $this->auto_render = FALSE;
    }

    public function action_load_more_messages() {
        $user_id = $_GET['UserId'];
        $last_msg_id = $_GET['LastMsgId'];
        $messages = Model::factory('Message')->get_more_messages_api($user_id, $last_msg_id);
        $sa = array();
        foreach ($messages as $message):
            $message->message = preg_replace('!\(?[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]\)?!', '', $message->message);
            $sa[] = $message->as_array();
        endforeach;
        $this->response->headers('Content-Type', 'application/json');
        echo '{"messages":' . json_encode($sa) . '}';
        $this->auto_render = FALSE;
    }

    public function action_readmessage_api() {

//            $message_id ="297";
//            $parent_message_id = "297";
        $message_id = $_GET['MessageId'];
        $parent_message_id = $message_id;
        $user_id = $_GET['UserId'];
        $messages = ORM::factory('Message')->where('message_id', '=', $message_id)->or_where('parent_message_id', '=', $parent_message_id)->find_all();
        foreach ($messages as $message):
            if ($message->receiver_id == $user_id):
                $message->is_message_viewed = 1;
                $message->save();
            endif;
        endforeach;
    }

    public function action_replymessage_api() {

        $user = $_POST['SenderId'];
        $message_id = $_POST['MessageId'];
        $parent_message_id = $_POST['ParentMessageId'];
        $message = $_POST['Message'];
//              $character_id = $_GET['CharacterId'];
        $receiver_id = $_POST['ReceiverId'];
//              $subject = $_GET['Subject'];
//              $user = "31";
//              $message_id = "182";
//              $parent_message_id = "182";
//              $_GET['message'] = "Testing Message";
        $_POST['CharacterId'] = "NULL";
//              $_GET['receiver'] = "32";
        $_POST['Subject'] = "Reply Message";

        if ($parent_message_id == "null" || $parent_message_id == "") {
            $_POST['ParentMessageId'] = $message_id;
            $_POST['is_message_parent'] = '1';
        } else {
            $messages = ORM::factory('Message')->where('message_id', '=', $parent_message_id)->or_where('user_id', '=', $user)->and_where('parent_message_id', '=', $parent_message_id)->and_where('is_message_active', '=', 1)->find_all();
            foreach ($messages as $message):
                $message->is_message_parent = '0';
                $message->save();
            endforeach;
            $_POST['ParentMessageId'] = $parent_message_id;
            $_POST['is_message_parent'] = '1';
        }
        $reply_message = Model::factory("Message")->add_reply_message_api($user, $_POST);
        $message = ORM::factory('Message')
                ->select('zid_users.*', 'zid_character_details.*', 'zid_games.*')
                ->join('zid_users', 'INNER')
                ->on('zid_users.id', '=', 'message.user_id')
                ->join('zid_character_details', 'LEFT OUTER')
                ->on('message.character_detail_id', '=', 'zid_character_details.character_detail_id')
                ->join('zid_games', 'LEFT OUTER')
                ->on('zid_character_details.game_id', '=', 'zid_games.id')
                ->where('message.message_id', '=', $reply_message)
                ->find();
//              $data = '{"replymessage":[{ "message_id":"'.$message->message_id.'","user_id":"'.$message->user_id.'","character_detail_id":"'.$message->character_detail_id.'","receiver_id":"'.$message->receiver_id.'","event_id":"'.$message->event_id.'","parent_message_id":"'.$message->parent_message_id.'","subject":"'.$message->subject.'","message":"'.$message->message.'","is_message_viewed":"'.$message->is_message_viewed.'","is_message_parent":"'.$message->is_message_parent.'","is_garbage_message":"'.$message->is_garbage_message.'","is_message_deleted":"'.$message->is_message_deleted.'","is_message_active":"'.$message->is_message_active.'","created_date":"'.$message->created_date.'","username":"'.$message->username.'","character_name":"'.$message->character_name.'","game_name":"'.$message->game_name.'" }]}';
        Pushnotification::send($message->user_id, $message->receiver_id, $message->subject, $message->message, $_POST['ParentMessageId']);
        $message->message = str_replace("\n", '\n', $message->message);        
        $data = '{"replymessage":[{ "message_id":"' . $message->message_id . '","user_id":"' . $message->user_id . '","character_detail_id":null,"receiver_id":"' . $message->receiver_id . '","event_id":null,"parent_message_id":"' . $message->parent_message_id . '","subject":"' . $message->subject . '","message":"' . $message->message . '","is_message_viewed":"' . $message->is_message_viewed . '","is_message_parent":"' . $message->is_message_parent . '","is_garbage_message":null,"is_message_deleted":null,"is_message_active":"' . $message->is_message_active . '","created_date":"' . $message->created_date . '","username":"' . $message->username . '","character_name":null,"game_name":null }]}';
        echo $data;
        //echo  preg_replace( "/\r\n|\r\n|\r|<br>|/", "\n", $data );
    }
    
    public function action_get_thread_messages() {
        $parent_message_id = $_GET['parentmessageid'];
        $messages = ORM::factory('Message')
                ->select('zid_users.username', 'zid_character_details.character_name', 'zid_games.game_name')
                ->join('zid_users', 'INNER')
                ->on('zid_users.id', '=', 'message.user_id')
                ->join('zid_guilds', 'LEFT OUTER')
                ->on('message.guild_id', '=', 'zid_guilds.guild_id')
                ->join('zid_events', 'LEFT OUTER')
                ->on('message.event_id', '=', 'zid_events.event_id')
                ->join('zid_character_details', 'LEFT OUTER')
                ->on('message.character_detail_id', '=', 'zid_character_details.character_detail_id')
                ->join('zid_games', 'LEFT OUTER')
                ->on('zid_character_details.game_id', '=', 'zid_games.id')
                ->where('message.parent_message_id', '=', $parent_message_id)
                ->or_where('message.message_id', '=', $parent_message_id) 
                ->order_by('message.created_date', 'DESC')
                ->find_all();
        $messages->as_array();
        foreach ($messages as $message):
            $message->message = preg_replace('!\(?[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]\)?!', '', $message->message);
            $sa[] = $message->as_array();
        endforeach;
        
        $this->response->headers('Content-Type', 'application/json');
        echo '{"messages":' . json_encode($sa) . '}';
        $this->auto_render = FALSE;
    }

    public function action_registration_device() {
        $user_id = $_GET['userid'];
        $device_id = $_GET['deviceID'];
        $platform = $_GET['platform'];
	if(!$user_id || !$device_id || !$platform){
            Log::instance()->add(Log::ERROR, 'missing params for one of device_id: ' . $device_id . ' for user' . $user_id . ' for platform: ' . $platform);
	    echo "Device registration failed missing params";
	}else{ 
        	if (!Model::factory("Device")->check_device_id($user_id, $device_id)) {
            		Model::factory("Device")->add_device($user_id, $device_id, $platform);
            		Log::instance()->add(Log::INFO, 'Registering device_id: ' . $device_id . ' for user' . $user_id . ' for platform: ' . $platform);
			echo "Device registration SUCCESS";
        	}else{
            		Log::instance()->add(Log::INFO, 'SKIPPING Registration. Already registered for device_id: ' . $device_id . ' for user' . $user_id . ' for platform: ' . $platform);
		}
	}
    }

}
